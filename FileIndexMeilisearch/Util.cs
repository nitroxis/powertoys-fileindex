﻿using System.Diagnostics;
using System.Drawing.Imaging;
using System.IO;
using System.IO.Pipelines;
using System.Windows;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace FileIndexMeilisearch;

static class Util
{
	private static readonly string[] suffixes = ["B", "KiB", "MiB", "GiB", "TiB", "PiB", "EiB", "ZiB", "YiB"];

	public static string FormatSize(ulong bytes)
	{
		int i = 0;
		double size = bytes;
		while (size >= 1024 && i < suffixes.Length - 1)
		{
			size /= 1024;
			i++;
		}

		return $"{size:0.##} {suffixes[i]}";
	}

	public static bool OpenInExplorer(string? path)
	{
		if (path == null)
			return false;

		ProcessStartInfo info = new() { FileName = "explorer" };
		info.ArgumentList.Add("/select,");
		info.ArgumentList.Add(path.Replace('/', '\\'));
		Process.Start(info);
		return true;
	}

	public static bool Open(string? path)
	{
		if (path == null)
			return false;

		ProcessStartInfo info = new()
		{
			FileName = path,
			UseShellExecute = true
		};
		Process.Start(info);
		return true;
	}

	public static bool CopyToClipboard(string? value)
	{
		if (value != null)
		{
			System.Windows.Clipboard.SetText(value);
		}

		return true;
	}

	public static ImageSource? GetIcon(string path)
	{
		var pipe = new Pipe();

		Task.Run(() =>
		{
			try
			{
				Icon? icon = null;
				try
				{
					icon = Icon.ExtractAssociatedIcon(path);
				}
				catch (FileNotFoundException)
				{
				}

				if (icon == null)
					return;

				using (icon)
				{
					Bitmap bm = icon.ToBitmap();
					bm.Save(pipe.Writer.AsStream(), ImageFormat.Png);
				}
			}
			finally
			{
				pipe.Writer.Complete();
			}
		});

		BitmapImage image = new();
		image.BeginInit();
		image.StreamSource = pipe.Reader.AsStream();
		image.EndInit();
		return image;
	}
}