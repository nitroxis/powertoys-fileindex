using System.Text.Json.Serialization;

namespace FileIndexMeilisearch;

// Keep in sync with https://gitlab.com/wtwrp/fileindex/-/blob/master/fileindex-vfs/src/search.rs

struct VfsSearchMime
{
	[JsonPropertyName("type")]
	public string Type { get; set; }

	[JsonPropertyName("subtype")]
	public string Subype { get; set; }

	public override string ToString()
	{
		return $"{this.Type}/{this.Subype}";
	}
}