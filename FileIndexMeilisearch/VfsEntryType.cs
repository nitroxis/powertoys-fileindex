namespace FileIndexMeilisearch;

// Keep in sync with https://gitlab.com/wtwrp/fileindex/-/blob/master/fileindex-vfs/src/search.rs

enum VfsEntryType
{
	File = 1,
	Directory = 2,
	Symlink = 3,
}