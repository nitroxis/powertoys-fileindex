using System.Text.Json.Serialization;

namespace FileIndexMeilisearch;

// Keep in sync with https://gitlab.com/wtwrp/fileindex/-/blob/master/fileindex-vfs/src/search.rs

class VfsSearchEntry
{
	[JsonPropertyName("id")]
	public ulong Id { get; set; }

	[JsonPropertyName("name")]
	public string? Name { get; set; }

	[JsonPropertyName("path")]
	public string? Path { get; set; }

	[JsonPropertyName("type")]
	public VfsEntryType Type { get; set; }

	[JsonPropertyName("meta")]
	public VfsMetadata Metadata { get; set; }

	[JsonPropertyName("mime")]
	public VfsSearchMime? Mime { get; set; }
}