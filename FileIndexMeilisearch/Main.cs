﻿using System.Text;
using System.Windows.Input;
using ManagedCommon;
using Meilisearch;
using Microsoft.PowerToys.Settings.UI.Library;
using Wox.Plugin;
using Wox.Plugin.Logger;
using Key = System.Windows.Input.Key;

namespace FileIndexMeilisearch;

/// <summary>
/// Main class of this plugin that implement all used interfaces.
/// </summary>
public class Main : IPlugin, IContextMenu, ISettingProvider, IDisposable
{
	private MeilisearchClient? client;

	public static string PluginID => "eef39fb0446842a3b9380ee2db2d2591";
	public string Name => "FileIndex Meilisearch";
	public string Description => "Search for files in a Meilisearch index created by FileIndex";

	private PluginInitContext? Context { get; set; }
	private string? IconPath { get; set; }
	private bool Disposed { get; set; }

	public IEnumerable<PluginAdditionalOption> AdditionalOptions =>
	[
		new()
		{
			Key = nameof(this.MeilisearchUrl),
			DisplayLabel = "Meilisearch URL",
			DisplayDescription = "URL of the Meilisearch server",
			PluginOptionType = PluginAdditionalOption.AdditionalOptionType.Textbox,
			TextValue = this.MeilisearchUrl,
		},
		new()
		{
			Key = nameof(this.MeilisearchKey),
			DisplayLabel = "Key",
			DisplayDescription = "Meilisearch API key",
			PluginOptionType = PluginAdditionalOption.AdditionalOptionType.Textbox,
			TextValue = this.MeilisearchKey,
		},
		new()
		{
			Key = nameof(this.MeilisearchIndex),
			DisplayLabel = "Index",
			DisplayDescription = "The name of the index to search",
			PluginOptionType = PluginAdditionalOption.AdditionalOptionType.Textbox,
			TextValue = this.MeilisearchIndex,
		},
		new()
		{
			Key = nameof(this.PathPrefix),
			DisplayLabel = "Prefix",
			DisplayDescription = "Prefix paths returned by Meilisearch with this string",
			PluginOptionType = PluginAdditionalOption.AdditionalOptionType.Textbox,
			TextValue = this.PathPrefix,
		}
	];

	private string MeilisearchUrl { get; set; } = "http://localhost:7700";
	private string MeilisearchKey { get; set; } = "";
	private string MeilisearchIndex { get; set; } = "data";
	private string PathPrefix { get; set; } = "";

	public List<Result> Query(Query query)
	{
		Log.Info($"Query: {query.Search}", this.GetType());

		List<Result> results = new();

		try
		{
			if (this.client == null)
			{
				this.client = new MeilisearchClient(this.MeilisearchUrl, this.MeilisearchKey);
			}

			SearchQuery q = new()
			{
				Limit = 30,
			};

			if (this.client.Index(this.MeilisearchIndex).SearchAsync<VfsSearchEntry>(query.Search, q).Result is { } response)
			{
				StringBuilder tooltip = new();
				foreach (var entry in response.Hits)
				{
					tooltip.Clear();
					if (entry.Metadata.Length > 0)
						tooltip.AppendLine($"Size: {Util.FormatSize(entry.Metadata.Length)}");
					if (entry.Metadata.Created > 0)
						tooltip.AppendLine($"Created: {DateTimeOffset.FromUnixTimeMilliseconds(entry.Metadata.Created).ToLocalTime()}");
					if (entry.Metadata.Modified > 0)
						tooltip.AppendLine($"Modified: {DateTimeOffset.FromUnixTimeMilliseconds(entry.Metadata.Modified).ToLocalTime()}");
					if (entry.Mime != null)
						tooltip.AppendLine($"Type: {entry.Mime}");

					entry.Path = this.PathPrefix + entry.Path;
					results.Add(new()
					{
						QueryTextDisplay = query.Search,
						Title = entry.Name,
						SubTitle = entry.Path,
						ToolTipData = new ToolTipData(entry.Name, tooltip.ToString()),
						ContextData = entry,
						Icon = entry.Type == VfsEntryType.Directory ? null : () => Util.GetIcon(entry.Path),
						Action = _ => Util.Open(entry.Path),
					});
				}
			}
		}
		catch (Exception ex)
		{
			results.Clear();
			results.Add(new()
			{
				QueryTextDisplay = query.Search,
				Title = "Meilisearch error",
				SubTitle = ex.Message,
				ToolTipData = new ToolTipData("Error", ex.ToString()),
			});
		}


		return results;
	}

	public void Init(PluginInitContext context)
	{
		Log.Info("Init", this.GetType());

		this.Context = context ?? throw new ArgumentNullException(nameof(context));
		this.Context.API.ThemeChanged += this.OnThemeChanged;
		this.updateIconPath(this.Context.API.GetCurrentTheme());
	}

	public List<ContextMenuResult> LoadContextMenus(Result selectedResult)
	{
		Log.Info("LoadContextMenus", this.GetType());

		if (selectedResult?.ContextData is VfsSearchEntry entry)
		{
			return
			[
				new ContextMenuResult
				{
					PluginName = this.Name,
					Title = "Open in Explorer (Shift+Enter)",
					FontFamily = "Segoe Fluent Icons,Segoe MDL2 Assets",
					Glyph = "\xE838", // FolderOpen
					AcceleratorKey = Key.Enter,
					AcceleratorModifiers = ModifierKeys.Shift,
					Action = _ => Util.OpenInExplorer(entry.Path),
				},
				new ContextMenuResult
				{
					PluginName = this.Name,
					Title = "Copy path to clipboard (Ctrl+C)",
					FontFamily = "Segoe Fluent Icons,Segoe MDL2 Assets",
					Glyph = "\xE8C8", // Copy
					AcceleratorKey = Key.C,
					AcceleratorModifiers = ModifierKeys.Control,
					Action = _ => Util.CopyToClipboard(entry.Path),
				},
			];
		}

		return [];
	}

	public System.Windows.Controls.Control CreateSettingPanel() => throw new NotImplementedException();

	public void UpdateSettings(PowerLauncherPluginSettings settings)
	{
		Log.Info("UpdateSettings", this.GetType());

		if (settings.AdditionalOptions.SingleOrDefault(x => x.Key == nameof(this.MeilisearchUrl))?.TextValue is { } url)
			this.MeilisearchUrl = url;

		if (settings.AdditionalOptions.SingleOrDefault(x => x.Key == nameof(this.MeilisearchKey))?.TextValue is { } key)
			this.MeilisearchKey = key;

		if (settings.AdditionalOptions.SingleOrDefault(x => x.Key == nameof(this.MeilisearchIndex))?.TextValue is { } index)
			this.MeilisearchIndex = index;

		if (settings.AdditionalOptions.SingleOrDefault(x => x.Key == nameof(this.PathPrefix))?.TextValue is { } pathPrefix)
			this.PathPrefix = pathPrefix;

		this.client = null;
	}

	public void Dispose()
	{
		Log.Info("Dispose", this.GetType());

		this.Dispose(true);
		GC.SuppressFinalize(this);
	}

	protected virtual void Dispose(bool disposing)
	{
		if (this.Disposed || !disposing)
		{
			return;
		}

		if (this.Context?.API != null)
		{
			this.Context.API.ThemeChanged -= this.OnThemeChanged;
		}

		this.Disposed = true;
	}

	private void updateIconPath(Theme theme) =>
		this.IconPath = theme is Theme.Light or Theme.HighContrastWhite
			? this.Context?.CurrentPluginMetadata.IcoPathLight
			: this.Context?.CurrentPluginMetadata.IcoPathDark;

	private void OnThemeChanged(Theme currentTheme, Theme newTheme) => this.updateIconPath(newTheme);
}