using System.Text.Json.Serialization;

namespace FileIndexMeilisearch;

// Keep in sync with https://gitlab.com/wtwrp/fileindex/-/blob/master/fileindex-vfs/src/search.rs

struct VfsMetadata
{
	[JsonPropertyName("modified")]
	public long Modified { get; set; }

	[JsonPropertyName("created")]
	public long Created { get; set; }

	[JsonPropertyName("len")]
	public ulong Length { get; set; }
}